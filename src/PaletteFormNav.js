import React, {Component} from 'react';
import clsx from 'clsx';
import {Link} from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AddToPhotosIcon from '@material-ui/icons/AddToPhotos';
import Button from '@material-ui/core/Button';
import PaletteMetaForm from "./PaletteMetaForm";
import useStyles from "./styles/PaletteFormNavStyles";

const PaletteFormNav = (props) => {

    const classes = useStyles();
    const {
        open,
        palettes,
        newPaletteName,
        handleDrawerOpen,
        handleSavePalette,
        handlePaletteNameChange
    } = props

    return (
        <>
            <CssBaseline/>
            <AppBar
                color="default"
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton,
                            open && classes.hide)}>
                        <AddToPhotosIcon/>
                    </IconButton>
                    <div className={classes.container}>
                        <Typography variant="h6" noWrap>
                            Create Palette
                        </Typography>
                        <div>
                            <PaletteMetaForm
                                palettes={palettes}
                                newPaletteName={newPaletteName}
                                handleSavePalette={handleSavePalette}
                                handlePaletteNameChange={handlePaletteNameChange}
                            />
                            <Link to="/">
                                <Button
                                    variant="contained"
                                    color="secondary">
                                    Go Back
                                </Button>
                            </Link>
                        </div>
                    </div>
                </Toolbar>
            </AppBar>
        </>
    );
}

export default PaletteFormNav;