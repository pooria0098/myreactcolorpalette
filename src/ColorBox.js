import React, {Component} from 'react';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {Link} from 'react-router-dom';
import {withStyles} from "@material-ui/core";
import styles from './styles/ColorBoxStyles';

class ColorBox extends Component {
    state = {
        copied: false
    }

    handleCopy = () => {
        this.setState({copied: true}, () => {
            setTimeout(() =>
                this.setState({copied: false}), 1500)
        })
    }

    render() {
        const {background, name, moreUrl, showLink, classes} = this.props
        return (
            <CopyToClipboard text={background} onCopy={this.handleCopy}>
                <div className={classes.colorBox}>
                    <div className={`${classes.colorBoxCopy} 
                        ${this.state.copied ? classes.colorBoxOverlay : ''}`}>
                        {this.state.copied
                            ? <>
                                <h1>Copied!</h1>
                                <p>{background}</p>
                            </>
                            : <button className={`${classes.colorBoxBtn} ${classes.colorBoxBtnCopy}`}>
                                Copy
                            </button>
                        }
                    </div>
                    <div className={classes.colorBoxContext}>
                        <span className={classes.lightColor}>{name.toUpperCase()}</span>
                        {showLink && <Link to={moreUrl} onClick={e => e.stopPropagation()}>
                            <button className={classes.colorBoxBtn}>More</button>
                        </Link>}
                    </div>
                </div>
            </CopyToClipboard>
        )
    }
}

export default withStyles(styles)(ColorBox);