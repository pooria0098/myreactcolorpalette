import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import styles from './styles/MiniPaletteStyles';
import DeleteIcon from "@material-ui/icons/Delete";

const MiniPalette = (props) => {
    // console.log('palette: ', props.palette)
    const {classes, openDialog} = props
    const {paletteName, emoji, colors, id} = props.palette

    const renderColorBoxes = () => (
        colors.map(color => (
            <div key={color.name} className={classes.color}
                 style={{background: color.color}}>&nbsp;</div>
        ))
    )

    const handleClick = (event) => {
        event.stopPropagation()
        openDialog(id)
    }

    return (
        <div className={classes.root} onClick={() => props.goToPalette(id)}>
            <div className={classes.delete}>
                <DeleteIcon
                    onClick={handleClick}
                    className={classes.deleteIcon}/>
            </div>
            <div className={classes.colors}>
                {renderColorBoxes()}
            </div>
            <div className={classes.context}>
                <span className={classes.palette}>{paletteName}</span>
                <span className={classes.emoji}>{emoji}</span>
            </div>
        </div>
    );
};

export default withStyles(styles)(MiniPalette);
