import React, {Component} from 'react';
import Palette from "./Palette";
import seedColors from "./seedColors";
import {generatePalette} from "./colorHelper";
import {Route, Switch} from "react-router-dom";
import PaletteList from "./PaletteList";
import SingleColorPalette from "./SingleColorPalette";
import NewPaletteForm from './NewPaletteForm';

class App extends Component {
    constructor(props) {
        super(props);
        const savedPalettes = JSON.parse(localStorage.getItem('palettes'))
        this.state = {palettes: savedPalettes || seedColors}
    }

    findPalette = (id) => {
        return this.state.palettes.find((palette) => {
            return palette.id === id
        })
    }

    savePalette = (newPalette) => {
        this.setState(
            {palettes: [...this.state.palettes, newPalette]},
            this.syncToLocalStorage
        )
    }

    deletePalette = (paletteId) => {
        this.setState({
            palettes: this.state.palettes.filter(palette => palette.id !== paletteId)
        }, this.syncToLocalStorage)
    }

    syncToLocalStorage = () => {
        localStorage.setItem('palettes', JSON.stringify(this.state.palettes))
    }

    render() {
        // console.log('palettes in App: ', this.state.palettes)
        return (
            <Switch>
                <Route
                    exact
                    path="/palette/new"
                    render={(routeProps) => <NewPaletteForm
                        savePalette={this.savePalette}
                        palettes={this.state.palettes}
                        {...routeProps}/>}/>
                <Route
                    exact
                    path="/"
                    render={(routeProps) =>
                        <PaletteList
                            deletePalette={this.deletePalette}
                            palettes={this.state.palettes}
                            {...routeProps}/>}/>
                <Route
                    exact
                    path="/palette/:id"
                    render={(routeProps) =>
                        <Palette palette={generatePalette(
                            this.findPalette(
                                routeProps.match.params.id))}/>}/>
                <Route
                    exact
                    path="/palette/:paletteId/:colorId"
                    render={(routeProps) =>
                        <SingleColorPalette
                            colorId={routeProps.match.params.colorId}
                            palette={generatePalette(
                                this.findPalette(
                                    routeProps.match.params.paletteId))}/>}/>
            </Switch>
        );
    }
}

export default App;