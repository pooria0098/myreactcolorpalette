import React, {Component} from 'react';
import ColorBox from "./ColorBox";
import Navbar from "./Navbar";
import Footer from "./PaletteFooter";
import {withStyles} from "@material-ui/core";
import styles from "./styles/PaletteStyles";

class Palette extends Component {
    state = {
        level: 500,
        format: 'hex'
    }

    onChangeLevel = (level) => {
        this.setState({level})
    }

    onChangeFormat = (format) => {
        this.setState({format})
    }

    render() {
        const {colors, paletteName, emoji, id} = this.props.palette
        const {classes} = this.props
        const {level, format} = this.state
        const renderBoxes = colors[level].map(color => (
            <ColorBox
                key={color.id}
                background={color[format]}
                name={color.name}
                showLink={true}
                moreUrl={`/palette/${id}/${color.id}`}/>
        ))

        return (
            <div>
                <Navbar
                    showSlider={true}
                    level={level}
                    onChangeLevel={this.onChangeLevel}
                    onChangeFormat={this.onChangeFormat}/>

                <div className={classes.paletteColor}>
                    {renderBoxes}
                </div>

                <Footer
                    paletteName={paletteName}
                    emoji={emoji}/>
            </div>
        );
    }
}

export default withStyles(styles)(Palette);