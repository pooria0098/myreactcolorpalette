import React, {Component} from 'react';
import Slider from "rc-slider";
import {Link} from 'react-router-dom';
import 'rc-slider/assets/index.css';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {withStyles} from "@material-ui/core";
import styles from "./styles/NavbarStyles";

class Navbar extends Component {
    state = {
        format: 'hex',
        open: false
    }

    handleChange = async (evt) => {
        await this.setState({format: evt.target.value, open: true})
        this.props.onChangeFormat(this.state.format)
    }

    closeSnackbar = () => {
        this.setState({open: false})
    }

    render() {
        const {level, onChangeLevel, showSlider, classes} = this.props
        const {format, open} = this.state
        return (
            <header className={classes.navBar}>
                <div className={classes.logoContainer}>
                    <Link className={classes.logo} to="/">reactColorPicker</Link>
                </div>
                {showSlider && <div className={classes.sliderContainer}>
                    <span>level: {level}</span>
                    <div className={classes.slide}>
                        <Slider
                            defaultValue={level}
                            min={100}
                            max={900}
                            step={100}
                            onAfterChange={onChangeLevel}/>
                    </div>
                </div>}
                <div className={classes.selectContainer}>
                    <Select value={format} onChange={this.handleChange}>
                        <MenuItem value="hex">HEX - #ffffff</MenuItem>
                        <MenuItem value="rgb">RGB - rgb 255 255 255</MenuItem>
                        <MenuItem value="rgba">RGBA - rgba 255 255 255 1.0</MenuItem>
                    </Select>
                </div>
                <Snackbar
                    open={open}
                    autoHideDuration={3000}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    onClose={this.closeSnackbar}
                    message={<span id="message-id">Format Changed To {format.toUpperCase()}</span>}
                    action={[
                        <IconButton
                            size="medium"
                            aria-label="close"
                            color="inherit"
                            onClick={this.closeSnackbar}>
                            <CloseIcon fontSize="medium"/>
                        </IconButton>
                    ]}
                />
            </header>
        );
    }
}

export default withStyles(styles)(Navbar);