import React, {Component} from 'react';
import MiniPalette from "./MiniPalette";
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import {blue} from "@material-ui/core/colors";
import {red} from '@material-ui/core/colors'
import {withStyles} from '@material-ui/core/styles';
import styles from './styles/PaletteListStyles';
import {Link} from 'react-router-dom';

class PaletteList extends Component {
    state = {
        openDeleteDialog: false,
        deletedPaletteId: ''
    }

    openDialog = (id) => {
        this.setState({
            openDeleteDialog: true,
            deletedPaletteId: id
        })
    }

    closeDialog = () => {
        this.setState({
            openDeleteDialog: false,
            deletedPaletteId: ''
        })
    }

    goToPalette = paletteId => {
        // console.log('paletteId: ', paletteId)
        this.props.history.push(`/palette/${paletteId}`)
    }

    handleDelete = (id) => {
        this.props.deletePalette(id)
        this.closeDialog()
    }

    render() {
        const {palettes, classes} = this.props
        // console.log('palettes: ', palettes)
        return (
            <div className={classes.root}>
                <div className={classes.container}>
                    <nav className={classes.nav}>
                        <h1>React Colors</h1>
                        <Link to="/palette/new">Create Palette</Link>
                    </nav>
                    <header className={classes.paletteContainer}>
                        {palettes.map(palette => (
                            <MiniPalette
                                key={palette.id}
                                // deletePalette={deletePalette}
                                openDialog={this.openDialog}
                                palette={palette}
                                goToPalette={this.goToPalette}/>
                        ))}
                    </header>
                </div>
                <Dialog
                    open={this.state.openDeleteDialog}
                    onClose={this.closeDialog}
                    aria-labelledby="simple-dialog-title">
                    <DialogTitle id="simple-dialog-title">Delete A Palette</DialogTitle>
                    <List>
                        <ListItem button onClick={() => this.handleDelete(this.state.deletedPaletteId)}>
                            <ListItemAvatar>
                                <Avatar style={{
                                    backgroundColor: blue[100],
                                    color: blue[600],
                                }}>
                                    <CheckIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary='Delete'/>
                        </ListItem>
                        <ListItem button onClick={this.closeDialog}>
                            <ListItemAvatar>
                                <Avatar style={{
                                    backgroundColor: red[100],
                                    color: red[600],
                                }}>
                                    <CloseIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="Cancel"/>
                        </ListItem>
                    </List>
                </Dialog>
            </div>
        )
    }
}

export default withStyles(styles)(PaletteList);