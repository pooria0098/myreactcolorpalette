import React, {Component} from 'react';
import {withStyles} from "@material-ui/core";
import styles from './styles/PaletteFooterStyles';

class PaletteFooter extends Component {
    render() {
        const {paletteName, emoji, classes} = this.props
        return (
            <footer className={classes.paletteFooter}>
                <span>{paletteName}</span>
                <span className={classes.emoji}>{emoji}</span>
            </footer>
        );
    }
}

export default withStyles(styles)(PaletteFooter);