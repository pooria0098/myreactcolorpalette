import React from 'react';
import clsx from 'clsx';
import DraggableColorList from './DraggableColorList';
import ColorPickerForm from "./ColorPickerForm";
import PaletteFormNav from "./PaletteFormNav";
import {useTheme} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Button from '@material-ui/core/Button';
import {arrayMove} from "react-sortable-hoc";
import useStyles from "./styles/NewPaletteFormStyles";

const NewPaletteForm = (props) => {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);

    const [colors, setColors] = React.useState(props.palettes[0].colors)
    const [newColorName, setNewColorName] = React.useState('')
    const [newPaletteName, setNewPaletteName] = React.useState('')

    const {maxColors, palettes} = props
    const paletteIsFull = colors.length >= maxColors

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const addNewColor = (currentColor, newColorName) => {
        const newColor = {
            color: currentColor,
            name: newColorName
        }
        setColors([...colors, newColor])
        setNewColorName('')
    }

    const handleColorNameChange = (event) => {
        setNewColorName(event.target.value)
    }

    const handlePaletteNameChange = (event) => {
        setNewPaletteName(event.target.value)
    }

    const handleSavePalette = (newPalette) => {
        newPalette.id = newPalette.paletteName.split(' ').join('-')
        newPalette.colors = colors
        props.savePalette(newPalette)
        props.history.push('/')
    }

    const removeColor = (colorName) => {
        console.log('colorName: ', colorName)
        setColors(colors.filter(color => color.name !== colorName))
    }

    const onSortEnd = ({oldIndex, newIndex}) => {
        setColors(arrayMove(colors, oldIndex, newIndex))
        // this.setState(({colors}) => ({
        //     colors: arrayMove(colors, oldIndex, newIndex),
        // }))
    }

    const clearPalette = () => {
        setColors([])
    }

    const addNewRandomColor = () => {
        const allColors = palettes.map(palette => palette.colors).flat()
        const rnd = Math.floor(Math.random() * allColors.length)
        const newColor = allColors[rnd]
        setColors([...colors, newColor])
    }

    return (
        <div className={classes.root}>
            <PaletteFormNav
                newPaletteName={newPaletteName}
                open={open}
                classes={classes}
                palettes={palettes}
                handleDrawerOpen={handleDrawerOpen}
                handleSavePalette={handleSavePalette}
                handlePaletteNameChange={handlePaletteNameChange}
            />
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}>
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
                    </IconButton>
                </div>
                <Divider/>
                <div className={classes.container}>
                    <Typography gutterBottom className={classes.title} variant="h4">
                        Design Your Palette
                    </Typography>
                    <div className={classes.buttons}>
                        <Button
                            variant="contained"
                            color="secondary"
                            className={classes.button}
                            onClick={clearPalette}>
                            Clear Palette
                        </Button>
                        <Button variant="contained"
                                color="primary"
                                className={classes.button}
                                disabled={paletteIsFull}
                                onClick={addNewRandomColor}>
                            Random Color
                        </Button>
                    </div>
                    <ColorPickerForm
                        colors={colors}
                        paletteIsFull={paletteIsFull}
                        newColorName={newColorName}
                        addNewColor={addNewColor}
                        handleColorNameChange={handleColorNameChange}
                    />
                </div>
            </Drawer>
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: open,
                })}>
                <div className={classes.drawerHeader}/>
                <div className={classes.boxContainer}>
                    <DraggableColorList
                        onSortEnd={onSortEnd}
                        colors={colors}
                        axis='xy'
                        distance={20}
                        removeColor={removeColor}/>
                </div>
            </main>
        </div>
    );
};

NewPaletteForm.defaultProps = {
    maxColors: 20
}

export default NewPaletteForm;