import {makeStyles} from "@material-ui/core/styles";
import {DRAWER_WIDTH} from "../constants";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        width: DRAWER_WIDTH,
        flexShrink: 0,
    },
    drawerPaper: {
        width: DRAWER_WIDTH,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        height:'91vh',
        flexGrow: 1,
        padding: 0,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -DRAWER_WIDTH,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
    boxContainer: {
        height:'100%',
        // display: 'grid',
        // gridTemplateColumns: 'repeat(5, minmax(20%, auto))',
        // gridTemplateRows: 'repeat(4, minmax(150px, auto))',
    },
    buttons: {
        width: '100%',
        marginBottom: '2rem'
    },
    button: {
        width: '50%',
        fontSize: '1.3rem',
    },
    title: {
        width: '100%',
        textAlign: 'center'
    },
    container: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '0 1rem',
    }
}))
export default useStyles