const styles = {
    root: {
        display: 'inline-block',
        width: '20%',
        height: '25%',
        backgroundColor: props => props.color,
        "&:hover svg": {
            color: 'white',
            transform: 'scale(1.5)'
        }
    },
    boxContent: {
        color: 'rgba(0,0,0,.5)',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        height: '100%',
        padding: ' 0 .3rem'
    },
    paletteName: {
        // padding: '.1rem',
        fontSize: '1.5rem',
        transition: 'all 2s ease-in-out'
    }
}
export default styles