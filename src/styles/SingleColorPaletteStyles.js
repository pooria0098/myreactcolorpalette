export default {
    singleColorPalette: {
        display: "grid",
        gridTemplateColumns: "repeat(5, minmax(100px, auto))",
        gridTemplateRows: "repeat(2, minmax(50px, 1fr))",
        width: "100vw",
        height: "89vh",
    },
    goBack: {
        backgroundColor: "black",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    colorBoxBtn: {
        padding: ".7rem 2rem",
        background: "rgba(255, 255, 255, .3)",
        textTransform: "uppercase",
        border: "none",
        fontSize: "1.7rem",
        textDecoration: "none",
        color: 'black',
    },
    colorBox: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        background: props => props.background,
        opacity: "1",
    },
}