export default {
    paletteFooter: {
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        fontSize: "2rem",
    },
    emoji: {
        fontSize: "3rem",
    }
}