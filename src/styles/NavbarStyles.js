export default {
    navBar: {
        display: "flex",
        height: "6vh",
        justifyContent: "space-between",
    },
    logoContainer: {
        flex: "2",
        maxWidth: "30rem",
        backgroundColor: "#ccc",
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: "1rem",
    },
    logo: {
        textDecoration: "none",
        color: "black",
        fontSize: "2rem",
        letterSpacing: "1px",
        fontFamily: "'Roboto', sans-serif",
    },
    sliderContainer: {
        flex: "6",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        padding: "1rem",
        "& span": {
            fontWeight: "500",
            fontSize: "1.8rem",
        }
    },
    slide: {
        width: "340px",
        margin: "0 10px",
        display: "inline-block",
        "& .rc-slider-handle, .rc-slider-handle:active, .rc-slider-handle:focus, .rc-slider-handle:hover": {
            background: "green",
            outline: "none",
            border: "2px solid green",
            boxShadow: "none",
            width: "13px",
            height: "13px",
            marginTop: "-3px",
        },
        "& .rc-slider-rail": {
            height: "8px",
        },
        "& .rc-slider-track": {
            background: "transparent",
        },
    },
    selectContainer: {
        flex: "2",
        maxWidth: "30rem",
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        width: "100%",
        padding: "1rem",
        "& .MuiInputBase-root": {
            fontSize: "2rem !important",
        }
    }
}