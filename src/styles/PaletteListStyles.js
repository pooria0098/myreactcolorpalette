export default {
    root: {
        backgroundColor: 'blue',
        height: '150vh'
    },
    container: {
        margin: '0 auto',
        maxWidth: '90rem',
        display: 'flex',
        flexDirection: 'column'
    },
    nav: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        color: 'white',
        margin: '1rem',
        "& a": {
            color: 'white',
            textDecoration: 'none'
        }
    },
    paletteContainer: {
        display: 'grid',
        gridTemplateColumns: 'repeat(3,30%)',
        gridGap: '5%',
    }
};