export default {
    root: {
        backgroundColor: 'white',
        borderRadius: '5px',
        overflow: 'hidden',
        display: 'flex',
        flexDirection: 'column',
        padding: '1rem',
        cursor: 'pointer',
        position: 'relative',
        "&:hover svg": {
            opacity: 1
        }
    },
    colors: {
        // margin: '1rem',
        borderRadius: '5px',
        overflow: 'hidden',
        display: 'grid',
        gridTemplateColumns: 'repeat(5,auto)',
        lineHeight: '2.5'
    },
    color: {},
    context: {
        // marginTop: '1rem',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    palette: {
        fontSize: '1.7rem',
        fontWeight: 'bold',
    },
    emoji: {
        fontSize: '3rem',
    },
    delete: {
        // width: '20px',
        // height: '20px',
    },
    deleteIcon: {
        color: '#fff',
        backgroundColor: 'orangered',
        width: '40px',
        height: '40px',
        // padding: '1rem',
        position: 'absolute',
        top: '0',
        right: '0',
        opacity: 0,
        transition: 'all .3s ease-in-out !important'
    }
};