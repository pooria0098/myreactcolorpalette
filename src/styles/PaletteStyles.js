export default {
    paletteColor: {
        display: "grid",
        gridTemplateColumns: "repeat(5, minmax(100px, auto))",
        gridTemplateRows: "repeat(4, minmax(50px, 1fr))",
        width: "100vw",
        height: "89vh",
    }
}