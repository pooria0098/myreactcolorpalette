const styles = {
    root: {
        //     width: '100%',
        //     height: '100%',
        //     display: 'flex',
        //     flexDirection: 'column',
        //     justifyContent: 'center',
        //     alignItems: 'center',
        // padding: '.5rem',
    },
    picker: {
        width: '100% !important'
    },
    pickerForm: {
        width: '100%',
        fontSize: '3rem !important'
    },
    pickerInput: {
        width: '100%',
        "& div": {
            fontSize: '2rem',
        }
    },
    pickerButton: {
        width: '100%',
        fontSize: '2rem'
    }
}
export default styles