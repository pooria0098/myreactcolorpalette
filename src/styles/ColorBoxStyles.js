import chroma from "chroma-js";

export default {
    colorBox: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        background: props => props.background,
        "&:hover button": {
            opacity: "1",
        }
    },
    colorBoxCopy: {
        background: props => props.background,
        flex: "1",
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-end",
        transform: "translateY(25%)",
        color: props => chroma(props.background).luminance() >= .4
            ? 'black' : 'white'
    },
    colorBoxOverlay: {
        position: "fixed",
        top: "-27rem",
        left: "0",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        width: "100vw",
        height: "100vh",
        color: "white",
        transform: "scale(2)",
        zIndex: "100",
        "& h1": {
            fontWeight: "400",
            textShadow: "1px 2px black",
            background: "rgba(255, 255, 255, 0.2)",
            width: "100%",
            textAlign: "center",
            marginBottom: "0",
            padding: "1rem",
            textTransform: "uppercase",
            transform: "translateY(200%)",
        },
        "& p": {
            fontSize: "2rem",
            fontWeight: "100",
            transform: "translateY(400%)",
        }
    },
    colorBoxBtn: {
        padding: ".7rem 2rem",
        background: "rgba(255, 255, 255, .3)",
        textTransform: "uppercase",
        border: "none",
        fontSize: "1.7rem",
        textDecoration: "none",
        color: props => chroma(props.background).luminance() >= .4
            ? 'black' : 'white'
    },
    colorBoxBtnCopy: {
        opacity: "0",
        transition: ".5s",
    },
    colorBoxContext: {
        flex: "1",
        display: "flex",
        alignItems: "flex-end",
        justifyContent: "space-between",
        width: "100%",
        "& span": {
            letterSpacing: "1px",
            padding: "1px 2px",
            color: props => chroma(props.background).luminance() <= .2
                ? 'white' : 'black'
        }
    },
}