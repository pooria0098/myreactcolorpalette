import React from 'react';
import DraggableColorBox from "./DraggableColorBox";
import {SortableContainer} from 'react-sortable-hoc';

const DraggableColorList = SortableContainer(
    ({colors, removeColor}) => {
        return (
            <div style={{height: "100%"}}>
                {colors.map((color, i) => (
                    <DraggableColorBox
                        index={i}
                        // index={`item-${index}`}
                        // index={index + Math.random()}
                        key={color.name}
                        handleDelete={() => removeColor(color.name)}
                        color={color.color}
                        name={color.name}/>
                ))}
            </div>
        );
    })

export default DraggableColorList;