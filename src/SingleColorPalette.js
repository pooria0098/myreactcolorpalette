import React, {Component} from 'react';
import ColorBox from "./ColorBox";
import Navbar from "./Navbar";
import Footer from "./PaletteFooter";
import {Link} from 'react-router-dom';
import {withStyles} from "@material-ui/core";
import styles from './styles/SingleColorPaletteStyles';

class SingleColorPalette extends Component {
    constructor(props) {
        super(props);
        this.state = {format: 'hex'}
        this.shades = this.gatherShades()
    }

    onChangeFormat = (format) => {
        this.setState({format})
    }

    gatherShades = () => {
        let shades = []
        let allColors = this.props.palette.colors
        const colorId = this.props.colorId
        for (let key in allColors) {
            shades = shades.concat(
                allColors[key].filter(color =>
                    color.id === colorId &&
                    !color.rgb.includes('rgb(255,255,255)'))
            )
        }
        // console.log('shades: ', shades)
        return shades
    }

    renderBoxes = () => this.shades.map(color => (
        <ColorBox
            key={color.name}
            background={color[this.state.format]}
            name={color.name}
            showLink={false}/>
    ))

    render() {
        const {paletteName, emoji, id} = this.props.palette
        const {classes} = this.props
        return (
            <div>
                <Navbar
                    showSlider={false}
                    onChangeFormat={this.onChangeFormat}/>

                <div className={classes.singleColorPalette}>
                    {this.renderBoxes()}
                    <div className={`${classes.goBack} ${classes.colorBox}`}>
                        <Link to={`/palette/${id}`} className={classes.colorBoxBtn}>
                            Go Back
                        </Link>
                    </div>
                </div>

                <Footer
                    paletteName={paletteName}
                    emoji={emoji}/>
            </div>
        );
    }
}

export default withStyles(styles)(SingleColorPalette);