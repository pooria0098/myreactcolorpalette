import React, {Component} from 'react';
import {ChromePicker} from 'react-color';
import Button from '@material-ui/core/Button';
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import {withStyles} from "@material-ui/core";
import styles from "./styles/ColorPickerFormStyles";


class ColorPickerForm extends Component {
    state = {
        currentColor: 'blue'
    }

    componentDidMount() {
        ValidatorForm.addValidationRule('isColorUnique', (value) => {
            this.props.colors.every(({color}) => color !== this.state.currentColor)
        });
        ValidatorForm.addValidationRule('isColorNameUnique', (value) => {
            this.props.colors.every(
                ({name}) => name.toLowerCase() !== value.toLowerCase()
            )
        });
    }

    handleColorChange = (newColor) => {
        console.log('newColor: ', newColor)
        this.setState({currentColor: newColor.hex})
    };

    render() {
        const {
            newColorName,
            paletteIsFull,
            addNewColor,
            handleColorNameChange,
            classes
        } = this.props
        const {currentColor} = this.state

        return (
            <div className={classes.root}>
                <ChromePicker
                    color={currentColor}
                    className={classes.picker}
                    onChangeComplete={this.handleColorChange}/>
                <ValidatorForm
                    // ref="form"
                    className={classes.pickerForm}
                    onSubmit={() => addNewColor(currentColor, newColorName)}
                    onError={errors => console.log('errors: ', errors)}>
                    <TextValidator
                        onChange={event => handleColorNameChange(event)}
                        value={newColorName}
                        name="newColorName"
                        className={classes.pickerInput}
                        placeholder="Color Name"
                        variant="filled"
                        validators={[
                            'required',
                            // 'isColorUnique',
                            // 'isColorNameUnique'
                        ]}
                        errorMessages={[
                            'Enter a color name',
                            // 'Color already used!',
                            // 'Color name must be unique'
                        ]}/>
                    <Button
                        variant="contained"
                        color="primary"
                        className={classes.pickerButton}
                        type="submit"
                        disabled={paletteIsFull}
                        style={{backgroundColor: paletteIsFull ? 'grey' : currentColor}}>
                        {paletteIsFull ? 'Palette Full' : 'Add Color'}
                    </Button>
                </ValidatorForm>
            </div>
        );
    }
}

export default withStyles(styles)(ColorPickerForm);