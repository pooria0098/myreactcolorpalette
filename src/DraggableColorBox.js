import React from 'react';
import {withStyles} from "@material-ui/styles";
import DeleteIcon from '@material-ui/icons/Delete';
import {SortableElement} from 'react-sortable-hoc';
import styles from "./styles/DraggableColorBoxStyles";

const DraggableColorBox = SortableElement((props) => {
    const {classes, name, handleDelete} = props
    return (
        <div className={classes.root}>
            <div className={classes.boxContent}>
                <span className={classes.paletteName}>{name}</span>
                <DeleteIcon onClick={handleDelete}/>
            </div>
        </div>
    );
})

export default withStyles(styles)(DraggableColorBox);