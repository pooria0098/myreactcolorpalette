import React, {useEffect} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import 'emoji-mart/css/emoji-mart.css';
import {Picker} from 'emoji-mart';

function PaletteMetaForm(props) {
    const [open, setOpen] = React.useState(false)
    const [stage, setStage] = React.useState('form')

    const {
        palettes,
        handlePaletteNameChange,
        newPaletteName,
        handleSavePalette
    } = props

    useEffect(() => {
        ValidatorForm.addValidationRule("isPaletteNameUnique", value =>
            palettes.every(
                ({paletteName}) => paletteName.toLowerCase() !== value.toLowerCase()
            )
        );
    })

    const changeStage = () => {
        setStage('emoji')
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleFormClose = () => {
        setOpen(false);
    };

    const handleEmojiClose = () => {
        setOpen(false);
        setStage('form')
    };

    const savePalette = (newEmoji) => {
        handleSavePalette({
            paletteName: newPaletteName,
            emoji: newEmoji.native
        })
    }

    return (
        <div>
            <Button variant="contained"
                    color="primary"
                    onClick={handleClickOpen}>
                Save
            </Button>
            <Dialog open={stage === 'emoji' && open} onClose={handleEmojiClose}>
                <DialogTitle id="form-dialog-title">Choose An Emoji</DialogTitle>
                <Picker onSelect={savePalette} title="pick a palette emoji"/>
            </Dialog>
            <Dialog open={stage === 'form' && open} onClose={handleFormClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Choose A PaletteName</DialogTitle>
                <ValidatorForm
                    onSubmit={changeStage}
                    onError={errors => console.log('errors: ', errors)}>
                    <DialogContent>
                        <DialogContentText>
                            To subscribe to this website, please enter your email address here. We will send updates
                            occasionally.
                        </DialogContentText>
                        <TextValidator
                            onChange={event => handlePaletteNameChange(event)}
                            value={newPaletteName}
                            name="newPaletteName"
                            validators={[
                                'required',
                                'isPaletteNameUnique'
                            ]}
                            fullWidth
                            margin="normal"
                            errorMessages={[
                                'Enter a color name',
                                'Palette name must be unique'
                            ]}/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleFormClose} color="primary">
                            Cancel
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit">
                            Save Palette
                        </Button>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        </div>
    );
}

export default PaletteMetaForm;